FROM node:latest
RUN npm install -g @angular/cli
ENV PATH /app/node_modules/.bin:$PATH
WORKDIR /app
COPY package.json /app/package.json
RUN npm install
COPY . /app
